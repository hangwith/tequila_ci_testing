#README

Wes Wise  
09/12/2014

##Overview

The purpose of this project is to develop a feature-rich Continuous Integration (CI) testing
for Tequila.

As of the time of this writing, this project is minimal in that it relies on a few Node.js packages  
for performing tests.

##Required Tools

This project requires the following tools to be installed on one's local machine (assuming OS X) is the
platform being used.

* Homebrew - package manager to install git or any other libraries
* Node.js + npm (node package manager comes with Node.js)
* Mocha (installed via npm)

This project relies on a few dependencies. The following is a description on what these dependencies do.

+ [Mocha](http://visionmedia.github.io/mocha/) - javascript test framework/runner
+ [Chai](http://chaijs.com/) - BDD/TDD assertion library
+ [SuperAgent](http://visionmedia.github.io/superagent/) - flexible, concise, ajax library for performing REST API calls

##How to Setup

In terminal, run the following commands:

+ Run ```ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"``` to install Homebrew
+ Goto http://nodejs.org/ to download the package installer for Node.js
+ Install mocha globally by ```npm install -g mocha```
+ Pull project via git
+ ```cd``` into the project directory
+ Run ```npm install``` to install node dependencies for the project; these are installed in the node_modules folder
+ Run ```mocha``` to run all test suites inside the test folder


##Project Structure

+ README.md - this file
+ lib - config files (for future use)
+ node_modules - locally installed node packages
+ package.json - node.js config file
+ test - folder containing test suites

##Defining Tests With Mocha

Mocha is a test runner/structure scheme for defining test suites and test cases. Mocha comes with a handful of functions for defining these tests.

* describe() - used to label a test suite and to group multiple test cases within that suite
* it() - used to provide a definition of a single test case that should pass or fail.  This function takes an anonymous function as a callback
* before() - hook function, will execute exactly once within a describe block
* beforeEach() - hook function, will execute before every it() defined in a describe block
* after() - hook function, will execute exactly once after all test cases have executed in describe block
* afterEach() - hook function, will execute after every it() defined in a describe block

Functions for items 3 to 6 are useful for console.log() statements or for performing initializing of data before running a test.  

A sample a few of the above functions is below.

```js
describe('Array', function(){
  before(function(){
    // ...
  });

  describe('#indexOf()', function(){
    it('should return -1 when not present', function(){
      [1,2,3].indexOf(4).should.equal(-1);
    });
  });
});
```

###A Note About Asynchronous Calls

Mocha will execute the test specs inside of a describe() in synchronous order.  This will NOT work for fetching remote data from a REST API.  A test spec will pass before the a response can make it back from the server. Mocha provides the done() for informing a test spec wait until data has been received. Every spec that is written using it() will have the following format:

```js
it('/GET retrieve a collection of users from the server', function(done) {
   request.get('http://api.somewhere.com/users)
   .end(function(err, res) {
       expect(res.body.objectId).to.equal('1WeEX4Naws');
       done();
   });
});
```

Mocha requires done to be passed in the callback fro it() and requires a call to done();  after a asynchronous function finishes.  This use of done() will be used in all test specs.

##Defining Assertions with Chai
Chai is an assertion library that is integrated into Mocha, but can be integrated into countless other test runners.  This current tool uses just one of several assertion schemes available, Expect.  Other assertions schemes include Should and Assert. Assert followings a more traditional style of structuring assertions in programming languages. Expect was chosen because it provides a more comfortable way of defining assertions that follow natural English. This allows test specs to be written in a manner that is easier for those with not a thorough programming background.
Refer to the example below for what Expect-style assertions look like:
 
```js
var expect = require('chai').expect
  , foo = 'bar'
  , beverages = { tea: [ 'chai', 'matcha', 'oolong' ] };

expect(foo).to.be.a('string');
expect(foo).to.equal('bar');
expect(foo).to.have.length(3);
expect(beverages).to.have.property('tea').with.length(3);
```

##Defining REST API Calls with SuperAgent
SuperAgent is an easy-to-use http request wrapper that sit on Node.  Performing a REST API call with Node's 'request' or 'http' is terribly difficult but requires writing more code than SuperAgent.  SuperAgent is concise, clean, and easy to read which is why it was chosen to performing REST API calls.
To make a simple /GET request, issue the following code. Here, request is a variable set to require('superagent');

```js
request
   .get('/search')
   .end(function(res){

 });
 ```
 
Note* the .end() is required. It sends off the request as well as closes the connection. SuperAgent does this automatically; there is no need to do it manually the traditional Node.js way.

###Common Functions for SuperAgent
The following is a list of common functions to be used with a SuperAgent request

* .get('string')	perform a /GET request | Ex	.get('/search')
* .query({hash})	perform a request with query parameters	| Ex .query({'sort': 'name', 'order': -1})
* .set('','')	set request headers | Ex	.set('Content-type': 'application/json')
* .send('hash')	send data via request body	| Ex .send('{"title":"My Novel", "order": 49}')
* .post('string')	perform /POST & /PUT request	
* .post('/somewhere') chained with .send() and .set()
* .del('string')	perform a DELETE request	| Ex .del('/user/283948234')
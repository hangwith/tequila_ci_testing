'use strict';

/*
 * Refer to https://hangwith.atlassian.net/wiki/display/PT/Admin
 */

var expect = require('chai').expect;
var superagent = require('superagent');

var options = {
	baseURL: 'dev.hangwith.com/v1',
	environment: 'dev'
};


describe('Moderation Endpoints', function() {
   
    describe('Retrive Moderation Activies', function() {
       
        it('/GET list of moderation activities for a user', function(done) {
            
            superagent.get('http://' + options.baseURL + '/users/moderation-activities')
                .query({userId: 'tFbNNCe4Dx'})
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    console.log(res.body);
                    done();
                });
        });
        
        it('/GET list of moderation activites for a user who does not exist', function(done) {
            
            superagent.get('http://' + options.baseURL + '/users/moderation-activities')
                .query({userId: 'foobar'})
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.empty;
                    console.log('Empty set is returned for activities for unkown user');
                    done();
                });
        });
    });
    
    
    describe.skip('Ban & Suspend User', function() {
    
       beforeEach(function() {
            //check for authentication 
       });
        
        it('/POST ban User', function(done) {
           
            superagent.post('http://' + options.baseURL + '/users/ban')
                .send({user: {objectId: 'userid'}, moderator: {objectId: 'objectId'}, reason: 'Profanity'})
                .end(function(err, res) {
                    expect(err).to.eql(null);
                
                    done();
                });
        });
        
        it('/POST ban user - unsuccessful');
        
        
        it('/POST suspend user', function(done) {
           
            superagent.post('http://' + options.baseURL + '/users/suspend')
            .query({durationInHours: 24})
            .send({user: {objectId: 'userid'}, moderator: {objectId: 'objectId'}, reason: 'My Reason'})
            .end(function(err, res) {
                expect(err).to.eql(null);
                expect(res.body).to.have.property('success');
                expect(res.body.success).to.eql('Successfully suspended user: ' + userId);
                
                done();
            });
        });
        
        it('/POST suspend user - unsuccessful');
        
        
        it('/POST unban user', function(done) {
            
            var userId = '';
           
            superagent.post('http://' + options.baseURL + '/users/unban/' + userId)
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.have.property('success');
                    expect(res.body.success).to.eql('Successfully unbanned user: ' + userId);
                    
                    done();
                });
        });
        
        it('/POST unban user - unsuccessful');
        
        
        it('/POST unsuspend user', function(done) {
            
            var userId = '';
           
            superagent.post('http://' + options.baseURL + '/users/unsuspend/' + userId)
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.have.property('success');
                    expect(res.body.success).to.eql('Successfully unsuspended user: ' + userId);
                    
                    done();
                });
        });
        
        it('/POST unsuspend user - unsuccessful');
    });
    
});


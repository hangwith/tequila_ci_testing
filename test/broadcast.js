'use strict';

/*
 * Refer to https://hangwith.atlassian.net/wiki/display/PT/Broadcasts
 */

var expect = require('chai').expect;
var superagent = require('superagent');

var options = {
	baseURL: 'https://dev-api.hangwith.com/v1',
	environment: 'dev'
};


describe('Broadcast Endpoints', function() {
   
    describe('Public/Anonymous actions', function() {
       
        it('/GET retrieve list of live broadcasts');
        
        it('/GET retriveve a list of broadcasts for a specified user');
            
    });
    
    describe.skip('/Private/Authenticated actions', function() {
       
        it('/GET retrieve a broadcast by its id', function(done) {
            
        });
        
        it('/POST start a broadcast', function(done) {
            
        });
        
        it('/GET stop a broadcast', function(done) {
            
        });
        
        it('/GET view a broadcast', function(done) {
            
        });
    });
});

'use strict';

/*
 * Refer to https://hangwith.atlassian.net/wiki/display/PT/DiscoverItem
 */

var expect = require('chai').expect;
var superagent = require('superagent');

var options = {
	baseURL: 'dev.hangwith.com/v1',
	environment: 'dev'
};

describe('Discover Endpoints', function() {
    
    this.timeout(10000);
   
    describe('Public/Anonymous CRUD operations', function() {
        
        it('/GET retrieve a list of Discover Items', function(done) {
            
            superagent.get('http://' + options.baseURL + '/discoverItem')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body[0]).to.have.property('@class', 'DiscoverItem');
                    expect(res.body[0]).to.have.property('rowOrder');
                    expect(res.body[0]).to.have.property('columnOrder');
                    console.log('Retrieved a list of discover items, count = ' + res.body.length);
                    done();
                });
        });    
        
        
        it('/GET retrieve a DiscoverItem by its id', function(done) {
           
            superagent.get('http://' + options.baseURL + '/discoverItem/mq0kihmS13')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.have.property('@class', 'DiscoverItem');
                    expect(res.body).to.have.property('rowOrder');
                    expect(res.body).to.have.property('columnOrder');
                    console.log('Retrieved discover item by id, name = ' + res.body.objectId);
                    done();
                });
        });
        
        
        it('/GET retrieve a list of DiscoverItems by category (All)', function(done) {
           
            superagent.get('http://' + options.baseURL + '/discoverItem?categoryId=Whs8ObJsNk')
                .query({loadObjects: true})
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body[0]).to.have.property('@class', 'DiscoverItem');
                    expect(res.body[0]).to.have.property('rowOrder');
                    expect(res.body[0]).to.have.property('columnOrder');
                    //first object in array should have a user property
                    expect(res.body[0]).to.have.property('user');
                    expect(res.body[0].user).to.have.property('@class', 'User');
                    //7th object in array should have property broadcast
                    expect(res.body[6]).to.have.property('broadcast');
                    expect(res.body[6].broadcast).to.have.property('@class', 'Broadcast');
                    console.log('Retrieved a list of discover items, count = ' + res.body.length);
                    done();
                });
        });
        
        it('/GET retrieve a list of DiscoverItems by category (All) and sort by rowOrder, columnOrder', function(done) {
           
            superagent.get('http://' + options.baseURL + '/discoverItem?categoryId=Whs8ObJsNk')
                .query({sortDirection: -1})
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body[0]).to.have.property('@class', 'DiscoverItem');
                    expect(res.body[0]).to.have.property('rowOrder');
                    expect(res.body[0]).to.have.property('columnOrder');
                    //console.log(res.body);
                    console.log('Retrieved a list of discover items, count = ' + res.body.length);
                    done();
                });
        });
        
        it('/GET retrieve a list of DiscoverItems by category that does not exist', function(done) {
           
            superagent.get('http://' + options.baseURL + '/discoverItem?categoryId=72983784')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body).to.be.empty;
                    console.log('Retrieved list of items that is in fact empty');
                    done();
            });
        });
    
    });
    
    
    /*
     * authenticated test cases
     */
    describe('Private/Authenticated CRUD Operations', function() {

        it('/POST create a new discoverItem');
        
        it('/GET new discoverItem');
        
        it('/GET verify that new discoverItem is in /GET list');
        
        it('/PUT update new discoverItem');
        
        it('/DELETE rmeove new disocverItem');
        
        
        describe('CRUD chain for DiscoverItem', function() {
           
            it('/POST create new disoverItem');
            
            it('/GET new discoverItem');
            
            it('/PUT update new discoverItem');
            
            it('/DELETE remove new discoverItem');
        });
    });
});

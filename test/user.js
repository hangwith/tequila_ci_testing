'use strict';

/*
 * Refer to https://hangwith.atlassian.net/wiki/display/PT/Users
 */

var expect = require('chai').expect;
var superagent = require('superagent');

var options = {
	baseURL: 'http://dev.hangwith.com/v1',
	environment: 'dev'
};


//group users
describe('User Endpoints', function() {
    
    this.timeout(10000);
    
    describe('Public/Anonymous CRUD operations', function() {   
        
        var userId = 'tFbNNCe4Dx';
        
        it('/GET retrieve user by id', function(done) {
           
            superagent.get(options.baseURL + '/users/' + userId)
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.have.property('@class', 'User');
                    expect(res.body.username).to.eql('MrWes');
                    console.log('Retrieved user, id = ' + res.body.objectId + ', username = ' + res.body.username);
                    done();
                });
        });
        
        it('/GET user by id - user\'s broadcasts', function(done) {
            
            superagent.get(options.baseURL + '/users/' + userId + '/broadcasts')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body.length).to.be.above(1);
                    expect(res.body[0]).to.have.property('@class', 'Broadcast');
                    expect(res.body[0]).to.have.property('viewer_count');
                    expect(res.body[0]).to.have.property('startTime');
                    console.log('Retrieved user\'s broadcasts, first broadcast id = ' + res.body[0].objectId);
                    done();
                });
        });
        
        it('/GET user by id - user\'s followers', function(done) {
            
            superagent.get(options.baseURL + '/users/' + userId + '/followers')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body.length).to.be.above(1);
                    expect(res.body[0]).to.have.property('@class', 'UserActivity');
                    expect(res.body[0]).to.have.property('type');
                    expect(res.body[0].toUserId).to.eql(userId);
                    console.log('Retrieved user\'s followers, first user id = ' + res.body[0].fromUserId);
                    done();
                });
        });
        
        it('/GET user by id - user\'s being followed/followings', function(done) {
            
            superagent.get(options.baseURL + '/users/' + userId + '/followings')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body.length).to.be.above(1);
                    expect(res.body[2]).to.have.property('@class', 'UserActivity');
                    expect(res.body[2]).to.have.property('type');
                    expect(res.body[2].fromUserId).to.eql(userId);
                    console.log('Retrieved user\'s being followed, first user id = ' + res.body[2].toUserId);
                    done();
                });
        });
    });
    
    
    /*
     * Authenticated endpoints
     */
    describe.skip('Private/Authenticated CRUD operations', function() {
        
        beforeEach(function() {
           //add authentication check 
        });

        it('Search for user by email - exact match', function(done) {

            superagent.get(options.baseURL + '/users?email=wisewes@gmail.com')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.be.an('array');
                    expect(res.body[0]).to.have.property('@class', 'User');
                    expect(res.body[0].email).to.not.eql(undefined);
                    expect(res.body[0].email).to.eql('wisewes@gmail.com');
                    console.log('found user by email: ' + res.body[0].username + ', email: ' + res.body[0].email);
                    done();
                });
        });

        it('/POST create new user with all required fields', function(done) {

            var userObject = {
                username: 'IAmSombodyWhooHoo',
                name: 'someone something',
                email: 'someone@yahoo.com',
                bcryptPassword: 'hang555555'
            };
            
            //perform validation check
            
            //perform post request
        });
        
        
        it('/POST create new user with missing required fields', function(done) {
           
            
        });

        
        it('/GET new user', function(done) {


        });

        it('/PUT update new user', function(done) {

        });

        it('/DELETE remove new user', function(data) {

        });

        describe('CRUD chain for User', function() {

            it('/POST create new user');

            it('/GET retrieve new user');

            it('/PUT update new user');

            it('/DELETE remove new user');
        });


        /*
         * change password
         */
        describe('Change user\'s password', function() {

            it('/PUT change the password for a user');
        });


        /*
         * user installations
         */
        describe('Find User Installations', function() {

            it('/GET find a list of device installations for a user with the provided objectId');
        });

    });
    
    
    /*
     * Search User Enpoints
     */
    describe('Searching Users Endpoints', function() {
       
        /*
         * search by username
         */
        describe('Search for user by username', function() {
           
            it('Search for user with username "wisewes"', function(done) {
               
                superagent.get(options.baseURL + '/users?username=wisewes4')
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body[0]).to.have.property('@class', 'User');
                        expect(res.body[0].username).to.eql('wisewes4');
                        done();
                    });
            });
            
            it('Search for user [starts with] username "josh', function(done) {
                
                superagent.get(options.baseURL + '/users?username=josh')
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.be.above(1);
                        expect(res.body[0]).to.have.property('@class', 'User');
                        done();
                    });
            });
        });
        
        
        /*
         * search by name
         */
        describe('Search for user by name', function() {
           
            it('Search for user by name starts with wes', function(done) {
                
                superagent.get(options.baseURL + '/users')
                    .query({name: 'wes'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.be.above(1);
                        expect(res.body[0]).to.have.property('@class', 'User');
                        expect(res.body[0]).to.have.property('active');
                        console.log('Retrieved users by query param name, contains "test", count =  ' + res.body.length);   
                        done();
                });
            });
            
            it('Search for user by name that starts with b', function(done) {
                
                superagent.get(options.baseURL + '/users')
                    .query({name: 'b'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.be.above(1);
                        expect(res.body[0]).to.have.property('@class', 'User');
                        expect(res.body[0]).to.have.property('active');
                        console.log('Retrieved users by query param name, starts with b, count = ' + res.body.length);
                        done();
                    });
            });
            
            it('Search for user by name that DOES NOT exist', function(done) {
               
                superagent.get(options.baseURL + '/users')
                    .query({name: '444'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body).to.be.empty;
                        console.log('Retrieved an empty array of users, user with name 444 does not exist');
                        done();
                    });
            });
            
            it('Search for user by name that DOES NOT exist and with page too large, expect error code', function(done) {
               
                superagent.get(options.baseURL + '/users') 
                    .query({name: '444', size: 2000, page: 200})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body.code).to.eql('400');
                        expect(res.body.error).to.eql('Page size exceed maximum query limit.');
                        console.log('Correctly recieved following error: ' + res.body.error);
                        done();
                    });
            });
        });
        
        
        /* 
         * search by username OR name
         */
        describe('Search for user by usernameOrName', function() {
            
            it('Search for user by username OR name that contains "test"', function(done) {
            
                superagent.get(options.baseURL + '/users')
                    .query({usernameOrName: 'test', size: 5})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body[0]).to.have.property('@class', 'User');
                    
                        //output
                        var objects = [];
                    
                        for(var i = 0; i < 5; i++) {
                            var results = res.body[i];
                            var obj = {name: results.name, username: results.username};
                            objects.push(obj);
                        }
                    
                        console.log('Correctly found users by username OR name: ' + JSON.stringify(objects));
                        done();
                    });
            });
            
            it('Search for user by username OR name that starts with ...', function(done) {
                
                superagent.get(options.baseURL + '/users')
                    .query({usernameOrName: 'book'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body).to.be.empty;
                        console.log('Expected result of usernameORName search is an empty array');
                        done();
                    });
            });
        });
        
        
        /*
         * user search by email
         */
        describe('Search for user by email', function() {
            
            it('Search for user by email - exact match', function(done) {
               
                superagent.get(options.baseURL + '/users?email=wisewes@gmail.com')
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body[0]).to.have.property('@class', 'User');
                        console.log('found user by email: ' + res.body[0].username);
                        
                        //email field should be NOT be included with public requests
                        expect(res.body[0].email).to.eql(undefined);
                        done();
                    });
            });
            
            it('Search for user by email - failing case', function(done) {
               
                superagent.get(options.baseURL + '/users?email=foobar@someplace.com')
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body).to.be.empty;
                        console.log('Did not find user by email address, empty array');
                        done();
                    });
            });
        });
        
    
        /* 
         * user search endpoints with 
         */
        describe('Search for users and fetch different size amount', function() {
           
            it('Search for users that have usernames starting with "a" and limit results to 25', function(done) {
               
                superagent.get(options.baseURL + '/users?username=a&size=25')
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.eql(25);
                        done();
                    });
            });
            
            it('Search for users that have usernames starting with "b" with a size of 4 and getting the 2nd set of results', function(done) {
               
                superagent.get(options.baseURL + '/users?username=b&size=4&page=2')
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body[0]).to.have.property('@class', 'User');
                        done();
                    });
            });
            
            it('Search for users with a name starting with "jos" limit results to 3', function(done) {
                
                superagent.get(options.baseURL + '/users?name=jos&size=3')
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.be.eql(3);
                        expect(res.body[0]).to.have.property('@class', 'User');
                        done();
                    });
            });
        });
        
        
        /* 
         * user validation endpoint
         */
        describe('/POST /users/validate', function() {
           
            it('Validate a user all fields included and all approrpriate lengths', function(done) {
               
                superagent.post(options.baseURL + '/users/validate')
                    .send({username: 'wisewes', email: 'wes@medlmobile.com', name: 'wiseweldksd', password: 'slkjd0283lfk'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body.success).to.eql('Successful User Validation');
                        done();
                    });
            });
            
            it('Attempt to validate user when name is missing', function(done) {
                
                superagent.post(options.baseURL + '/users/validate')
                    .send({username: 'wisewes', name: 'foobartoo', password: '203skdkfjls'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        //expect(res.body.success).to.eql('Successful User Validation');
                        expect(res.body.code).to.eql('400');
                        done();
                    });
            });
            
            it('Attempt to validate user when username contains less than three characters', function(done) {
                
                superagent.post(options.baseURL + '/users/validate')
                    .send({username: 'wi', email: 'wes@medlmobile.com', name: 'foobartoo', password: '203skdkfjls'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        //expect(res.body.success).to.eql('Successful User Validation');
                        expect(res.body.code).to.eql('400');
                        done();
                    });
            });
            
            it.skip('Attempt to validate with invalid email', function(done) {
               
                superagent.post(options.baseURL + '/users/validate')
                    .send({username: 'wisewes', email: 'wesmedlmobile.com', name: 'wiseweldksd', password: 'slkjd0283lfk'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body.success).to.eql('Successful User Validation');
                        console.log(res.body);
                        done();
                    });
            });

        });
        
        
        /*
         * search with ids
         */
        describe('/GET Get a list of users by their user ids', function() {
           
            it('Case: find with three known ids: ["tFbNNCe4Dx", "ibzCL5xIPY", "QzaKnk7mcm"]', function(done) {
               
                superagent.get(options.baseURL + '/users')
                    .query({ids: 'tFbNNCe4Dx,ibzCL5xIPY,QzaKnk7mcm'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.be.eql(3);
                        console.log('Found users by their ids: ' + res.body[0].username + ', ' + res.body[1].username + ', ' + res.body[2].username);
                        done();
                    });
            });
            
            
            it('Case: check results when no ids are passed', function(done) {
               
                superagent.get(options.baseURL + '/user')
                    .query({ids: ''})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.empty;
                        console.log('Result is indeed empty if not id is provided in query param');
                        done();
                    });
            }); 
            
            
            it('Case: check results when wrong id is passed', function(done) {
               
                superagent.get(options.baseURL + '/user')
                    .query({ids: '29384lskdf'})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.empty;
                        console.log('Result is empty when non-existant id is passed');    
                        done();
                    });
            });
            
        
            it('Case: check results when multiples of the same user id is passed', function(done) {
               
                var ids = 'tFbNNCe4Dx,tFbNNCe4Dx,tFbNNCe4Dx';
                
                superagent.get(options.baseURL + '/users')
                    .query({ids: ids})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.be.eql(1);
                        expect(res.body[0].username).to.eql('MrWes');
                        console.log('Result is only one user object if the same id is passed multiple times');
                        done();
                    });
            });
            
            
            it('Case: check results when more than max (100) user ids are passed', function(done) {
                
                //obtain a list of userids to create a param string greater than 100
                superagent.get(options.baseURL + '/users')
                    .query({username: 'a', size: 125})
                    .end(function(err, res) {
                        expect(err).to.eql(null);
                        expect(res.body).to.be.an('array');
                        expect(res.body.length).to.eql(125);
                    
                        var results = res.body;
                        var userIds = [];

                        results.forEach(function(elem, index, array) {
                            userIds.push(elem.objectId);
                        });
                    
                        var idsAsString = userIds.join(',');
                    
                        //take those list of ids to find users exceeding 100
                        superagent.get(options.baseURL + '/users')
                            .query({username: 'a', ids: idsAsString})
                            .end(function(err, res) {
                                expect(err).to.eql(null);
                                expect(res.body).not.to.be.an('array');
                                expect(res.body.code).to.eql('400');
                                expect(res.body.error).to.eql('IDs parameter exceeded maximum allowable size: 100');
                                console.log('Expected error code and message is sent when count of ids is greater than 100');
                                done();
                            });
                    }); 
                
            });
        });

    });

});

'use strict';

/*
 * Refer to https://hangwith.atlassian.net/wiki/display/PT/Category
 */

var expect = require('chai').expect;
var superagent = require('superagent');

var options = {
	baseURL: 'dev.hangwith.com/v1',
	environment: 'dev'
};


describe('Category Endpoints', function() {
    
    this.timeout(10000);
   
    describe('Public/Anonymous CRUD operations', function() {
       
        it('/GET retrieve a list of categories', function(done) {
            
            var count = null;
            
            superagent.get('http://' + options.baseURL + '/category')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body.length).to.be.above(0); 
                    console.log('Retrieved List of categories, count = ' + res.body.length);
                    done();
                });
        });
        
        it('/GET retrieve a catogory by its id (All)', function(done) {
            
            superagent.get('http://' + options.baseURL + '/category/Whs8ObJsNk')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.have.property('@class', 'Category');
                    expect(res.body.name).to.eql('All');
                    console.log('Retrieved category: ' + res.body.name); 
                    done();
                });
        });
        
        it('/GET retrieve a category by its id (Featured Artist)', function(done) {
            
            superagent.get('http://' +  options.baseURL + '/category/J7455OcnmL')
                .end(function(err, res) {
                    expect(err).to.eql(null);
                    expect(res.body).to.have.property('@class', 'Category');
                    expect(res.body.name).to.eql('Featured Artist');
                    console.log('Retrieved category: ' + res.body.name);
                    done();
                });
        });
    });
    
    
    /* 
     * authenticated test cases
     */
    describe.skip('Private/Authenticated CRUD operations', function() {
        
        beforeEach(function() {
           
            //perform authentication check
        });
       
               
        it('/POST create a new category', function(done) {
            
        });
        
        it('/PUT update new category', function(done) {
            
        });
        
        it('/DELETE remove new category', function(done) {
            
        });
        
        it('/POST create a new category to set as hidden', function(done) {
            
        });
        
        it('/DELETE remove new hidden category', function(done) {
            
        });
        
        
        
        describe.skip('CRUD chain for Category', function() {
            
            it('/POST create a new category', function(done) {
                
            });
            
            it('/GET recently created category', function(done) {
                
            });
            
            it('/GET verify created category appears in /GET all', function(done) {
                
            });
            
            it('/PUT update recently created category', function(done) {
                
            });
            
            it('/DELETE remove recently created category', function(done) {
                
            });
        });
    });
});

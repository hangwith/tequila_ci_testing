'use strict';

//var expect = require('chai').expect;
//var superagent = require('superagent');

var options = {
	baseURL: 'https://dev-api.hangwith.com/v1',
	environment: 'dev'
};

describe('Tequila REST API : Dev', function() {
	
	describe('Collection Family Category', function() {
	   
		it('/GET retrieves category collection', function(done) {
			superagent.get(options.baseURL + '/category')
				.end(function(err, res) {
					expect(res.body.length).to.be.above(0);
					//console.log(res.body);
					done();
				})
		});
		
		it('/GET retrieves category collection with query param of sort and order', function(done) {
			superagent.get(options.baseURL + '/category?sort=name&order=-1')
				.end(function(err, res) {
					expect(res.body.length).to.be.above(0);
					//console.log(res.body);
					done();
				});
		});
	
		it('/GET retrieve a single item id = 1WeEX4Naws (Film, TV & Comedy)', function(done) {
			superagent.get(options.baseURL + '/category/1WeEX4Naws')
				.end(function(err, res) {
					expect(res.body.objectId).to.equal('1WeEX4Naws');
					expect(res.body).to.have.property('@class');
					//console.log(res.body);
					done();
				});
		});
		
		it('[ STUB ] /POST create a new category', function(done) {
			done();
		});
		
		it('[ STUB ] /PUT update a category with id of ---', function(done) {
			done();
		});
		
		it('[ STUB ] /DELETE a category with id of ---', function(done) {
			done();
		});
	
	});
	
	
	
	describe('Collection Family Discover', function() {
		
		it('/GET retrieves discover collection', function(done) {
			superagent.get(options.baseURL + '/discoverItem')
				.end(function(err, res) {
					expect(res.body.length).to.be.above(0);
					expect(res.body[0]).to.have.property('@class');
					//console.log(res.body[0]);
					done();
				});
		});
	});
    
    
    describe('Collection Family Broadcast', function() {
        
        it('/GET retrieves the Live Broadcast collection', function(done) {
            superagent.get(options.baseURL + '/broadcasts/live')
                .end(function(err, res) {
                    console.log(res.body);
                done();
            });
        });
        
        
        it('[ STUB ] /GET retrieve a collection of broadcast from a user', function(done) {
             done();
        });
    });
	
});
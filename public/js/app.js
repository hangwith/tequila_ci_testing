//public/js/app.js

'use strict';

//dependency injection of controllers into app
angular.module('sampleApp', ['ngRoute', 'appRoutes', 'MainController', 'HelpController']);


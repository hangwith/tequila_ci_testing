// public/js/controllers/MainController.js

'use strict';

angular.module('MainController', []).controller('MainController', function($scope, $location) {
    
    $scope.aButton = function() {
        mocha.run();
    };
    
    $scope.refreshPage = function() {
        
        //$scope.reload();
        $location.path('/');
    };
});

//.directive('testDirective', function() {
//    
//    return {
//        templateUrl: '../views/test.html',
//        replace: true,
//        restrict: 'E'
//    };
//});
